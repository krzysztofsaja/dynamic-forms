import { NgModule } from '@angular/core';

import { NameComponent } from './name.component';
import { DynamicFormQuestionComponent } from '@app/shared/dynamic-forms/components/question-component/dynamic-form-question.component';
import { DynamicFormComponent } from '@app/shared/dynamic-forms/components/question-form-component/dynamic-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule, MatDatepickerModule,
  MatFormFieldModule, MatIconModule,
  MatInputModule, MatNativeDateModule, MatOptionModule, MatRadioModule,
  MatSelectModule,
  MatSliderModule, MatSlideToggleModule
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatOptionModule,
    FormsModule,
    ReactiveFormsModule],
  declarations: [DynamicFormQuestionComponent, DynamicFormComponent],
  exports: [DynamicFormComponent],
  providers: []
})
export class DynamicFormsModule {
}
