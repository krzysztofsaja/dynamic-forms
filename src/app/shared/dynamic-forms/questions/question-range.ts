import { QuestionBase } from './question-base';

export class RangeQuestion extends QuestionBase<string | number> {
  controlType = 'range';
  type: string;
  invert = 'invert';
  max: number;
  min: number;
  step: number;
  thumbLabel;
  tickInterval;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.max = options['max'] || 10;
    this.min = options['min'] || 1;
    this.step = options['step'] || 1;
    this.thumbLabel = options['thumbLabel'] || true;
    this.tickInterval = options['tickInterval'] || 1;
  }
}
