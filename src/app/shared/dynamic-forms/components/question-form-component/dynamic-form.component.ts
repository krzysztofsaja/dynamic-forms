import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { QuestionControlService } from '../question-component/question-control.service';
import { QuestionBase } from '../../questions/question-base';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ QuestionControlService ]
})
export class DynamicFormComponent implements OnInit, OnDestroy {
  @Output() onSubmitClick = new EventEmitter();
  @Output() onFormChange = new EventEmitter();
  @Input() questions: QuestionBase<any>[] = [];
  form: FormGroup;
  subscription: Subscription;

  constructor(private qcs: QuestionControlService) {  }

  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.questions);
    this.subscription = this.form.valueChanges.subscribe(values => this.onFormChange.emit(values));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  onSubmit() {
    this.onSubmitClick.emit(this.form.value);
  }
}
