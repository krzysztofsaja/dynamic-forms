import { Injectable } from '@angular/core';

import { DropdownQuestion } from './questions/question-dropdown';
import { QuestionBase } from './questions/question-base';
import { TextboxQuestion } from './questions/question-textbox';
import { RangeQuestion } from '@app/shared/dynamic-forms/questions/question-range';
import { RadioQuestion } from '@app/shared/dynamic-forms/questions/question-radio';

@Injectable()
export class QuestionMockService {

  getQuestions() {

    const questions: QuestionBase<any>[] = [

      new DropdownQuestion({
        key: 'brave',
        placeholder: 'Bravery Rating',
        options: [
          {key: 'solid',  value: 'Solid'},
          {key: 'great',  value: 'Great'},
          {key: 'good',   value: 'Good'},
          {key: 'unproven', value: 'Unproven'}
        ],
        order: 3
      }),
      new RadioQuestion({
        key: 'radioTest',
        placeholder: 'Radio test',
        options: [
          {key: 'solid',  value: 'Solid'},
          {key: 'great',  value: 'Great'},
          {key: 'good',   value: 'Good'},
          {key: 'unproven', value: 'Unproven'}
        ],
        order: 3
      }),

      new TextboxQuestion({
        key: 'firstName',
        label: 'First name',
        value: 'Bombasto',
        required: true,
        order: 1
      }),

      new TextboxQuestion({
        key: 'emailAddress',
        placeholder: 'john@tieto.com',
        type: 'email',
        order: 2
      }),
      new RangeQuestion({
        key: 'question3',
        label: 'Opinion',
        type: 'range',
        order: 3,
        value: 50,
        min: 1,
        max: 10,
        step: 1,
        placeholder: 'Decide about something'
      }),
      new RangeQuestion({
        key: 'question4',
        label: 'Opinion Second',
        type: 'range',
        order: 4,
        min: 1,
        max: 10,
        step: 1,
        required: true,
        placeholder: 'Decide about something'
      })
    ];

    return questions.sort((a, b) => a.order - b.order);
  }
}
