import { Component, OnInit } from '@angular/core';

import { routeAnimations } from '@app/core';
import { QuestionMockService } from '@app/shared/dynamic-forms/questions-mock.service';

@Component({
  selector: 'anms-examples',
  templateUrl: './examples.component.html',
  styleUrls: ['./examples.component.scss'],
  animations: [routeAnimations]
})
export class ExamplesComponent implements OnInit {
  examples = [
    { link: 'todos', label: 'Todos' },
    { link: 'stock-market', label: 'Stocks' },
    { link: 'theming', label: 'Theming' },
    { link: 'authenticated', label: 'Auth' }
  ];
  questions;

  constructor(private questionMockService: QuestionMockService) {
    this.questions = this.questionMockService.getQuestions();
  }

  ngOnInit() {}

  onFormSubmit(data) {
    console.log(data);
  }
}
