import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared';

import { StaticRoutingModule } from './static-routing.module';
import { AboutComponent } from './about/about.component';
import { FeaturesComponent } from './features/features.component';
import { DynamicFormsModule } from '@app/shared/dynamic-forms/dynamic-forms.module';

@NgModule({
  imports: [SharedModule, StaticRoutingModule, DynamicFormsModule],
  declarations: [AboutComponent, FeaturesComponent]
})
export class StaticModule {}
